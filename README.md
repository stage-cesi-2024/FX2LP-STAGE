# FX2LP-STAGE

## Les differents moyens de compilation utilisées

1. [Logiciel Keil.V5](#Keil_V5)
2. [Compilateur SDCC](#SDCC)
3. [VSCode et plugin EIDE](#EIDE)
4. [Ez-USB Suite (Eclipse)](#Eclipse)

---

### 1. Logiciel Keil.V5 <a name="Keil_V5"></a>

Tout d'abord, j'ouvre mon projet et mon fichier "Led.c", qui est mon fichier principal contenant le code suivant :

```c
#define ALLOCATE_EXTERN
#include "fx2regs.h"
#include "fx2types.h"
#include "delay.h"

void main(void) {
  OED=0xff;   // port D as output
  for(;;) {
    IOD=0x00; // all 8 pins low
    delay(500);
    IOD=0xff; // all 8 pins high
    delay(500);
  }
}
```

Ensuite, j'ai ajouté les en-têtes (headers) au projet, ici en version 2 de mes tests (`Test_KEILV2`) :

![1714399148933](Images/1714399148933.png)

Ces en-têtes sont inclus dans le SDK du microcontrôleur FX2LP.

Cependant, après avoir compilé le projet, j'obtiens les erreurs suivantes :

![1714399268139](Images/1714399268139.png)

L'erreur provient de la ligne "`EXTERN xdata volatile BYTE GPIF_WAVE_DATA AT 0xE400;`" qui est la première ligne du programme, mais l'erreur se répète tout du long.

Le code associé est le suivant :

```c
#ifdef ALLOCATE_EXTERN
#define EXTERN
#define AT at
#else
#define EXTERN extern
#define AT ;
#endif

EXTERN xdata volatile BYTE GPIF_WAVE_DATA    AT 0xE400;
EXTERN xdata volatile BYTE RES_WAVEDATA_END  AT 0xE480;
```

Cela semble indiquer qu'il ne comprend pas la syntaxe spécifique, malgré mes essais avec des exemples de code fournis par le fabricant du microcontrôleur.

Alors que pourtant dans les options du projet, le microcontrôleur FX2LP est correctement sélectionné :

![1714399655359](Images/1714399655359.png)

---

### 2. Compilateur SDCC <a name="SDCC"></a>

La compilation avec SDCC ne fonctionne pas non plus.

J'utilise la commande suivante dans l'invite de commande : `sdcc -mmcs51 --std-c99 fw.c`

voici les erreurs que j'obtiens :

```output
 make -k all
Building file: ../EZRegs.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"EZRegs.rel" "../EZRegs.c" && echo -n EZRegs.d ./ > EZRegs.d && sdcc -c -MM --model-small  "../EZRegs.c" >> EZRegs.d
Finished building: ../EZRegs.cBuilding file: ../USBJmpTb.asm
Invoking: SDCC Assembler
sdas8051 -losg  -o "USBJmpTb.rel" "../USBJmpTb.asm"
Finished building: ../USBJmpTb.asmBuilding file: ../bulkloop.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"bulkloop.rel" "../bulkloop.c" && echo -n bulkloop.d ./ > bulkloop.d && sdcc -c -MM --model-small  "../bulkloop.c" >> bulkloop.d
../bulkloop.c:16: fatal error: when writing output to : Broken pipe
   16 |
      |
compilation terminated.
../bulkloop.c:247: syntax error: token -> '0' ; column 34
subprocess error 1
make: *** [subdir.mk:54: bulkloop.rel] Error 1
Building file: ../delay.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"delay.rel" "../delay.c" && echo -n delay.d ./ > delay.d && sdcc -c -MM --model-small  "../delay.c" >> delay.d
Finished building: ../delay.cBuilding file: ../delayms.asm
Invoking: SDCC Assembler
sdas8051 -losg  -o "delayms.rel" "../delayms.asm"
Finished building: ../delayms.asmBuilding file: ../discon.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"discon.rel" "../discon.c" && echo -n discon.d ./ > discon.d && sdcc -c -MM --model-small  "../discon.c" >> discon.d
Finished building: ../discon.cBuilding file: ../dscr.asm
Invoking: SDCC Assembler
sdas8051 -losg  -o "dscr.rel" "../dscr.asm"
Finished building: ../dscr.asmBuilding file: ../fw.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"fw.rel" "../fw.c" && echo -n fw.d ./ > fw.d && sdcc -c -MM --model-small  "../fw.c" >> fw.d
../fw.c:213: warning 283: function declarator with no prototype
../fw.c:367: syntax error: token -> '6' ; column 35
make: *** [subdir.mk:54: fw.rel] Error 1
Building file: ../get_strd.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"get_strd.rel" "../get_strd.c" && echo -n get_strd.d ./ > get_strd.d && sdcc -c -MM --model-small  "../get_strd.c" >> get_strd.d
Finished building: ../get_strd.cBuilding file: ../i2c.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"i2c.rel" "../i2c.c" && echo -n i2c.d ./ > i2c.d && sdcc -c -MM --model-small  "../i2c.c" >> i2c.d
../i2c.c:121: syntax error: token -> '9' ; column 32
make: *** [subdir.mk:54: i2c.rel] Error 1
Building file: ../i2c_rw.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"i2c_rw.rel" "../i2c_rw.c" && echo -n i2c_rw.d ./ > i2c_rw.d && sdcc -c -MM --model-small  "../i2c_rw.c" >> i2c_rw.d
Finished building: ../i2c_rw.cBuilding file: ../resume.c
Invoking: SDCC Compiler
sdcc -c --model-small -o"resume.rel" "../resume.c" && echo -n resume.d ./ > resume.d && sdcc -c -MM --model-small  "../resume.c" >> resume.d
Finished building: ../resume.cBuilding file: ../susp.asm
Invoking: SDCC Assembler
sdas8051 -losg  -o "susp.rel" "../susp.asm"
Finished building: ../susp.asmmake: Target 'all' not remade because of errors.14:08:53 Build Finished (took 8s.227ms)
```

---

###  3. VSCode plus extension EIDE <a name="EIDE"></a>

Sur le code EIDE j'ai quelques alerte et quelques erreurs, ainsi qu'une erreur qui est liée à une taille de code trop elevée.

---

### 4. Ez-USB_Suite (Eclipse) <a name="Eclipse"></a>

Cette fois-ci, je tente le tout pour le tout en utilisant Eclipse et SDCC avec cette video : "[How to get started with developing for the Cypress EZ-USB FX2](https://www.youtube.com/watch?v=bpVk98my2yw)"

Le probleme que j'avais avant, est que Eclipse n'arrivais pas à faire fonctionner le fichier make.

J'ai réussis à l'installer à l'aide du logiciel Choco, à partir de la commande : `choco install make`

Malheureusement, cela produit les mêmes erreurs que précédement !

Toutes aides est bien venu, d'avance merci.

Contact : mathys.anselme[at]viacesi.fr
