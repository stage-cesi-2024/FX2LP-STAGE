#define ALLOCATE_EXTERN
#include "fx2regs.h"
#include "fx2types.h"
#include "delay.h"

void main(void) {
  OED=0xff; // port D as output
  for(;;) {
    IOD=0x00; // all 8 pins low
    delay(500);
    IOD=0xff; // all 8 pins high
    delay(500);
  }
}
